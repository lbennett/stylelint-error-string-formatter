# @lbennett/stylelint-error-string-formatter

`stringFormatter` from `stylelint` with a re-print of error messages on completion.

## Installation and usage

```bash
npm install --save-dev @lbennett/stylelint-error-string-formatter
# OR
yarn add --dev @lbennett/stylelint-error-string-formatter
```

Add to `stylelint` as a custom formatter using the [API](https://stylelint.io/user-guide/node-api/#formatter).

Or CLI...

```bash
stylelint.js src/* --custom-formatter node_modules/stylelint-error-string-formatter
```

